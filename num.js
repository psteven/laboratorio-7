window.onload = function(){
    document.getElementById('boton').onclick = function () {//Le pongo función al botón
        
        x0=0;//Declaro las variables de cada casilla
        x1=0;
        x2=0;
        x3=0;
        x4=0;
        x5=0;
        x6=0;
        x7=0;
        x8=0;

        i=1;//Contador numeros primos
        j=0;//Contador numero de casillas
        div=0;//Contador de divisores de los numeros

        x0=document.getElementById("num0").value//Obtengo el valor de los input
        x1=document.getElementById("num1").value
        x2=document.getElementById("num2").value
        x3=document.getElementById("num3").value
        x4=document.getElementById("num4").value
        x5=document.getElementById("num5").value
        x6=document.getElementById("num6").value
        x7=document.getElementById("num7").value
        x8=document.getElementById("num8").value

        x0=parseInt(x0);//Los convierto en enteros
        x1=parseInt(x1);
        x2=parseInt(x2);
        x3=parseInt(x3);
        x4=parseInt(x4);
        x5=parseInt(x5);
        x6=parseInt(x6);
        x7=parseInt(x7);
        x8=parseInt(x8);

        var valores = [x0,x1,x2,x3,x4,x5,x6,x7,x8];//Declaro un vector con todos los números

        while (j<valores.length)//Mientras j menor al tamaño del vector, para que con cada valor revise si es primo y si es par
        {
            while (i<=valores[j])//Voy revisando cada posición del vector, es decir, voy revisando cada número del vector
            {
                if (valores[j]%i==0)//Si el numero es divisor del contador i, aumento la variable divisor
                {
                    div++;//Aumento la variable de los divisores del numero
                }
                i++//Aumento el contador para continuar revisando divisores
            }
            document.write(valores[j]+"\n")//Escribo el numero que revise y un espacio
            if (div==2)//Es primo si solo tiene dos divisores (1 y el mismo numero)
            {
                document.write("Es primo")
            }
            else
            {
                document.write("No es primo")
            }
            div=0//Reinicio el contador de divisores para trabajar con el siguiente numero del vector
            i=1//Reinicio el contador para empezar a revisar desde 1 

            if (valores[j]%2==0)//Si %2 es 0 entonces es par
            {
                document.write ("\n"+" y es par."+"</br>")//Escribo espacio, si es par y un salto de líneas
            } 
            else
            {
                document.write ("\n"+"y es impar."+"</br>")
            }
            j++//Aumento el contador de las posiciones del vector para que cada vez que acabe el ciclo, tome el siguiente numero del vector
        }   
    }
}